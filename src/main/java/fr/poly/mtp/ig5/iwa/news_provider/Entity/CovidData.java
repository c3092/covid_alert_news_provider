package fr.poly.mtp.ig5.iwa.news_provider.Entity;

import com.opencsv.bean.CsvBindByName;

import java.util.Date;

public class CovidData {

    @CsvBindByName(column = "date")
    private String date;

    @CsvBindByName(column = "tx_pos")
    private double tx_pos;

    @CsvBindByName(column = "tx_incid")
    private double tx_incid;

    @CsvBindByName(column = "TO")
    private double TO;

    @CsvBindByName(column = "R")
    private double R;

    @CsvBindByName(column = "rea")
    private int rea;

    @CsvBindByName(column = "hosp")
    private int hosp;

    @CsvBindByName(column = "rad")
    private int rad;

    @CsvBindByName(column = "dchosp")
    private int dchosp;

    @CsvBindByName(column = "incid_rea")
    private int incid_rea;

    @CsvBindByName(column = "incid_hosp")
    private int incid_hosp;

    @CsvBindByName(column = "incid_rad")
    private int incid_rad;

    @CsvBindByName(column = "incid_dchosp")
    private int incid_dchosp;

    @CsvBindByName(column = "conf")
    private int conf;

    @CsvBindByName(column = "conf_j1")
    private int conf_j1;

    @CsvBindByName(column = "pos")
    private int pos;

    @CsvBindByName(column = "esms_dc")
    private int esms_dc;

    @CsvBindByName(column = "dc_tot")
    private int dc_tot;

    @CsvBindByName(column = "pos_7j")
    private int pos_7j;

    @CsvBindByName(column = "cv_dose1")
    private double cv_dose1;

    @CsvBindByName(column = "esms_cas")
    private int esms_cas;

    public CovidData() {
    }

    public CovidData(String date, double tx_pos, double tx_incid, double TO, double r, int rea, int hosp, int rad, int dchosp, int incid_rea, int incid_hosp, int incid_rad, int incid_dchosp, int conf, int conf_j1, int pos, int esms_dc, int dc_tot, int pos_7j, double cv_dose1, int esms_cas) {
        this.date = date;
        this.tx_pos = tx_pos;
        this.tx_incid = tx_incid;
        this.TO = TO;
        R = r;
        this.rea = rea;
        this.hosp = hosp;
        this.rad = rad;
        this.dchosp = dchosp;
        this.incid_rea = incid_rea;
        this.incid_hosp = incid_hosp;
        this.incid_rad = incid_rad;
        this.incid_dchosp = incid_dchosp;
        this.conf = conf;
        this.conf_j1 = conf_j1;
        this.pos = pos;
        this.esms_dc = esms_dc;
        this.dc_tot = dc_tot;
        this.pos_7j = pos_7j;
        this.cv_dose1 = cv_dose1;
        this.esms_cas = esms_cas;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getTx_pos() {
        return tx_pos;
    }

    public void setTx_pos(double tx_pos) {
        this.tx_pos = tx_pos;
    }

    public double getTx_incid() {
        return tx_incid;
    }

    public void setTx_incid(double tx_incid) {
        this.tx_incid = tx_incid;
    }

    public double getTO() {
        return TO;
    }

    public void setTO(double TO) {
        this.TO = TO;
    }

    public double getR() {
        return R;
    }

    public void setR(double r) {
        R = r;
    }

    public int getRea() {
        return rea;
    }

    public void setRea(int rea) {
        this.rea = rea;
    }

    public int getHosp() {
        return hosp;
    }

    public void setHosp(int hosp) {
        this.hosp = hosp;
    }

    public int getRad() {
        return rad;
    }

    public void setRad(int rad) {
        this.rad = rad;
    }

    public int getDchosp() {
        return dchosp;
    }

    public void setDchosp(int dchosp) {
        this.dchosp = dchosp;
    }

    public int getIncid_rea() {
        return incid_rea;
    }

    public void setIncid_rea(int incid_rea) {
        this.incid_rea = incid_rea;
    }

    public int getIncid_hosp() {
        return incid_hosp;
    }

    public void setIncid_hosp(int incid_hosp) {
        this.incid_hosp = incid_hosp;
    }

    public int getIncid_rad() {
        return incid_rad;
    }

    public void setIncid_rad(int incid_rad) {
        this.incid_rad = incid_rad;
    }

    public int getIncid_dchosp() {
        return incid_dchosp;
    }

    public void setIncid_dchosp(int incid_dchosp) {
        this.incid_dchosp = incid_dchosp;
    }

    public int getConf() {
        return conf;
    }

    public void setConf(int conf) {
        this.conf = conf;
    }

    public int getConf_j1() {
        return conf_j1;
    }

    public void setConf_j1(int conf_j1) {
        this.conf_j1 = conf_j1;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public int getEsms_dc() {
        return esms_dc;
    }

    public void setEsms_dc(int esms_dc) {
        this.esms_dc = esms_dc;
    }

    public int getDc_tot() {
        return dc_tot;
    }

    public void setDc_tot(int dc_tot) {
        this.dc_tot = dc_tot;
    }

    public int getPos_7j() {
        return pos_7j;
    }

    public void setPos_7j(int pos_7j) {
        this.pos_7j = pos_7j;
    }

    public double getCv_dose1() {
        return cv_dose1;
    }

    public void setCv_dose1(double cv_dose1) {
        this.cv_dose1 = cv_dose1;
    }

    public int getEsms_cas() {
        return esms_cas;
    }

    public void setEsms_cas(int esms_cas) {
        this.esms_cas = esms_cas;
    }

    @Override
    public String toString() {
        return "CovidData{" +
                "date=" + date +
                ", tx_pos=" + tx_pos +
                ", tx_incid=" + tx_incid +
                ", TO=" + TO +
                ", R=" + R +
                ", rea=" + rea +
                ", hosp=" + hosp +
                ", rad=" + rad +
                ", dchosp=" + dchosp +
                ", incid_rea=" + incid_rea +
                ", incid_hosp=" + incid_hosp +
                ", incid_rad=" + incid_rad +
                ", incid_dchosp=" + incid_dchosp +
                ", conf=" + conf +
                ", conf_j1=" + conf_j1 +
                ", pos=" + pos +
                ", esms_dc=" + esms_dc +
                ", dc_tot=" + dc_tot +
                ", pos_7j=" + pos_7j +
                ", cv_dose1=" + cv_dose1 +
                ", esms_cas=" + esms_cas +
                '}';
    }
}

