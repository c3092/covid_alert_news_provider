package fr.poly.mtp.ig5.iwa.news_provider.service;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import fr.poly.mtp.ig5.iwa.news_provider.Entity.CovidData;
import fr.poly.mtp.ig5.iwa.news_provider.Entity.News;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Service;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class NewsProviderService {

    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36";

    public NewsProviderService(){

    }

    public List<News> getNews() throws IOException, ParseException {
        final Document doc = Jsoup.connect("https://news.google.com/rss/topics/CAAqIggKIhxDQkFTRHdvSkwyMHZNREZqY0hsNUVnSm1jaWdBUAE?hl=fr&gl=FR&ceid=FR%3Afr").userAgent(USER_AGENT).get();


        List<News> listNews = new ArrayList<>();
        for( Element item : doc.select("item") )
        {
            final String title = Objects.requireNonNull(item.select("title").first()).text();
            final String link = Objects.requireNonNull(item.select("link").first()).text();
            final String date = Objects.requireNonNull(item.select("pubDate").first()).text();
            final String source = Objects.requireNonNull(item.select("source").first()).text();
            final String description = Objects.requireNonNull(item.select("description").first()).text();
            News news = new News(title,link,date,source,description);

            listNews.add(news);

        }

        return listNews;
    }

    public List<CovidData> getCovidData() throws Exception {

        return readCsvFile("https://www.data.gouv.fr/fr/datasets/r/f335f9ea-86e3-4ffa-9684-93c009d5e617");

    }

    private static List<CovidData> readCsvFile(String filePath) throws Exception {


        List<CovidData> data;

        trustAllCertificates();
        URL stockURL = new URL(filePath);
        BufferedReader in = new BufferedReader(new InputStreamReader(stockURL.openStream()));

        CsvToBean<CovidData> csvReader = new CsvToBeanBuilder<CovidData>(in)
                .withType(CovidData.class)
                .withSeparator(',')
                .withFieldAsNull(CSVReaderNullFieldIndicator.NEITHER)
                .withIgnoreLeadingWhiteSpace(true)
                .withIgnoreEmptyLine(true)
                .build();
        data = csvReader.parse();

        return data;
    }

    private static void trustAllCertificates() throws NoSuchAlgorithmException, KeyManagementException
    {
        TrustManager[] trustManagers = new TrustManager[]{new X509TrustManager()
        {
            public X509Certificate[] getAcceptedIssuers()
            {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(
                    X509Certificate[] certs, String authType)
            {
            }

            public void checkServerTrusted(
                    X509Certificate[] certs, String authType)
            {
            }
        }};

        SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, trustManagers, new SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
    }

}
