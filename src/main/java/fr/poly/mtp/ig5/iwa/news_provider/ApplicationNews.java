package fr.poly.mtp.ig5.iwa.news_provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "fr")
public class ApplicationNews {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationNews.class, args);
    }

}
