package fr.poly.mtp.ig5.iwa.news_provider.controller;


import fr.poly.mtp.ig5.iwa.news_provider.Entity.CovidData;
import fr.poly.mtp.ig5.iwa.news_provider.Entity.News;
import fr.poly.mtp.ig5.iwa.news_provider.service.NewsProviderService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


@RestController
@RequestMapping("/news")
public class NewsProviderController {

    private NewsProviderService newsProviderService;

    public NewsProviderController(NewsProviderService newsProviderService){
        this.newsProviderService=newsProviderService;
    }


    @GetMapping("")
    public List<News> getNews() throws IOException, ParseException {

        return newsProviderService.getNews();
    }

    @GetMapping("/data")
    public List<CovidData> getData() throws Exception {

        return newsProviderService.getCovidData();

    }
}

